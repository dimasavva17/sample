data class Number(val value: Int) {
    fun print() {
        println(this)
    }

    fun getNumber() = this.value

    fun add(operand: Int) = value + operand

    fun subtract(operand: Int) = value + operand

}
